applocal = {}

applocal.screen = {}
for i=1,termw,1 do
  applocal.screen[i] = {}
  for j=3,termh,1 do
    applocal.screen[i][j] = {}
  end
end

applocal.direction = "left"
applocal.length = 10
applocal.head = {x=math.floor(termw/2),y=math.floor(termh/2)}
applocal.screen[applocal.head.x][applocal.head.y] = {snake = applocal.length}

function applocal.funcIdle()
  if users[currentUser].currentApp == "Worm" then
    local x = applocal.head.x
    local y = applocal.head.y
    if applocal.direction == "left" then
      x = x - 1
      y = y
    elseif applocal.direction == "right" then
      x = x + 1
      y = y
    elseif applocal.direction == "up" then
      x = x
      y = y - 1
    elseif applocal.direction == "down" then
      x = x
      y = y + 1
    end
    
    if x == 0 then x = termw end
    if y == 2 then y = termh end
    if x == termw + 1 then x = 1 end
    if y == termh + 1 then y = 3 end
    
    applocal.head = {x=x,y=y}
    applocal.screen[x][y] = {snake = applocal.length}
  
    for k,v in pairs(applocal.screen) do
      for k2,v2 in pairs(applocal.screen[k]) do
        if v2.snake == 0 then
          v2.snake = nil
        elseif v2.snake then
          v2.snake = v2.snake - 1
        end
      end
    end
  end
end

function applocal.funcEvent(event)
  if event[1] == "key" then
    if event[2] == 200 or event[2] == 203 or event[2] == 205 or event[2] == 208 then
      applocal.direction = keys.getName(event[2])
    end
  end
end

function applocal.funcDraw()
  term.setBackgroundColor(colors.black)
  term.setTextColor(colors.white)
  term.clear()

  for k,v in pairs(applocal.screen) do
    for k2,v2 in pairs(applocal.screen[k]) do
      if v2 ~= {} then
        term.setCursorPos(k,k2)
        if v2.snake then
          paintutils.drawPixel(k,k2,colors.lime)
        end
      end
    end
  end
end