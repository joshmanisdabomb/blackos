applocal.camera = 0
applocal.layout = {}
applocal.folderLength = 0
applocal.clickableApps = {}

function applocal.funcIdle()
  if totalframes % 200 == 0 then
    for k,v in pairs(fs.list("Users/" .. userlocal:getName() .. "/Data/HomeLayout")) do
      applocal.layout[k] = {}
      applocal.layout[k].realname = v
      applocal.layout[k].name = File.getExtension(v)
      applocal.layout[k].camera = 0
      for k2,v2 in pairs(fs.list(fs.combine("Users/" .. userlocal:getName() .. "/Data/HomeLayout", v))) do
        applocal.layout[k][k2] = {name = File.getExtension(v2), icon = paintutils.loadImage(fs.combine("System/Data/Icons", File.getExtension(v2) .. ".ico"))}
      end
      applocal.folderLength = math.max(applocal.folderLength,string.len(File.getExtension(v)))
    end
  end
end

function applocal.funcEvent(event)
  if event[1] == "mouse_click" then
    if event[3] <= applocal.folderLength+2 then
      if applocal.layout[math.floor((event[4]+3-applocal.camera)/6)] then
        objectDragged = {"homeFolder",math.floor((event[4]+3-applocal.camera)/6),event[3],event[4]}
      else
        objectDragged = {"homeFolder",nil,nil,event[4]}
      end
    else
      for k,v in pairs(applocal.clickableApps) do
        if event[3] >= v.x1 and event[3] <= v.x2 and event[4] >= v.y1 and event[4] <= v.y2 then
          User.currentUser():setApp(k)
        end
      end
    end
  elseif event[1] == "mouse_drag" then
    if objectDragged[1] == "homeFolder" then
      if objectDragged[3] then
        if event[3] < objectDragged[3] then
          objectDragged[4] = nil
          applocal.layout[objectDragged[2]].camera = applocal.layout[objectDragged[2]].camera - 1
        elseif event[3] > objectDragged[3] then
          objectDragged[4] = nil
          applocal.layout[objectDragged[2]].camera = applocal.layout[objectDragged[2]].camera + 1
        end
      end
      if objectDragged[4] then
        if event[4] < objectDragged[4] then
          objectDragged[3] = nil
          applocal.camera = applocal.camera - 1
        elseif event[4] > objectDragged[4] then
          objectDragged[3] = nil
          applocal.camera = applocal.camera + 1
        end
      end
      if objectDragged[3] then objectDragged[3] = event[3] end
      if objectDragged[4] then objectDragged[4] = event[4] end
    end
  elseif event[1] == "mouse_scroll" then
    applocal.camera = applocal.camera + -event[2]
  end
end

function applocal.funcDraw()
  term.setBackgroundColor(colors.black)
  term.setTextColor(colors.white)
  term.clear()
  
  applocal.clickableApps = {}
  
  local y = 1
  for k,v in pairs(applocal.layout) do
    local x = applocal.folderLength+3+v.camera
    for k2,v2 in pairs(v) do
      if type(k2) == "number" then
        applocal.clickableApps[v2.name] = {}
        applocal.clickableApps[v2.name].x1 = x
        applocal.clickableApps[v2.name].y1 = y*6-3+applocal.camera
        applocal.clickableApps[v2.name].y2 = applocal.clickableApps[v2.name].y1 + 4
        
        paintutils.drawImage(v2.icon,x,applocal.clickableApps[v2.name].y1)
        local cursorPos = {term.getCursorPos()}
        for i=0,-4,-1 do
          term.setCursorPos(cursorPos[1],cursorPos[2]+i)
          term.write(string.rep(" ", string.len(v2.name)+1))
        end
        term.setCursorPos(cursorPos[1],cursorPos[2]-1)
        term.write(v2.name .. " ")
        x = term.getCursorPos()+1
        
        applocal.clickableApps[v2.name].x2 = x - 2
      end
    end
    
    term.setBackgroundColor(colors.black)
    for i=-3,1,1 do
      term.setCursorPos(1,y*6+i+applocal.camera)
      term.write(string.rep(" ", applocal.folderLength+2))
    end
    
    term.setCursorPos(2,y*6-1+applocal.camera)
    term.write(v.name)
    
    y = y + 1
  end
end